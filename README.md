# lds-autozip

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://gitlab.com/loodos/library/lds-autozip/blob/master/LICENSE)

lds-autozip is a PowerShell script that automatically compresses old log files to save space.

You can run this script on the terminal or create an automated scheduled task.

Archive files are created / grouped by year-month name (eg 2018-04.zip). So that you can easily find the old files in the archived files according to the naming.

## Configuration

| Parameter        | Mandatory | Default                       | Description                                                                         |
| ---------------- | --------- | ----------------------------- | ----------------------------------------------------------------------------------- |
| beforeDays       | false     | 30                            | Set to archive files older than x days only                                         |
| fileFormat       | false     | *.txt                         | Set to archive files that only match this format.                                   |
| backupFolderName | false     | backup                        | Set the folder name in which the archived files are created.                        |
| zipFileSuffix    | false     | \<empty>                      | The name of the archive files is defined by date (yyyy-MM). You can set the suffix. |
| deleteAfter      | false     | false                         | After archiving, you can permanently delete the file.                               |
| sevenZipExePath  | false     | C:\Program Files\7-Zip\7z.exe | Set the location of the 7-Zip exe file on your computer.                            |

## Usage

`.\lds-autozip.ps1 -beforeDays 30 -fileFormat lds-*.log -zipFileSuffix "lds" -deleteAfter $True`

> In the above sample; files that are older than `30 days` and the file name starts with `lds-` and extension is `log` will be archived with the name of `2018-04-lds.zip` and will be `delete`d later.

`.\lds-autozip.ps1 -beforeDays 10 -fileFormat *.txt -backupFolderName "old" -deleteAfter $false`

> In the above sample; files that are older than `10 days` and extension is `txt` will be archived with the name of `.\old\2018-04.zip`.

## Creating a scheduling task

You can automate achive process with a scheduling task that runs every day.

```
The task must be `run as Administrator user or group` and `run with highest privileges` for deletion.
```

### Create new Action

| | |
|-|-|
| Action | Start a Program |
| Program/script | PowerShell |
| Add arguments | `.\lds-autozip.ps1 -beforeDays 10 -deleteAfter $false` |
| Start in | `C:\Loodos\ps-scripts\` |
| | |

### Create new Trigger

| | |
|-|-|
| Begin the task | On a schedule |
| Daily | Recur every `1` days |
| | |

## Who is using this

Here are companies, teams, projects or libraries that use this project in their work.

- [Loodos](https://loodos.com)
- [Vodafone TR, Yanımda Mobile App](https://vodafone.com.tr)
- [Beşiktaş, Vodafone Park Mobile App](http://www.vodafonearena.com.tr/)
- and more...

Please add your own! Send a Pull Request or [contact us](https://loodos.com/#letsconnect).

Thanks!

## Commercial Support

This is an open source project, so the amount of time we have available to help resolve your issue is often limited as all help is provided on a volunteer basis. If you want to get priority help, need to get up to speed quickly, require some training or mentoring, or need full 24 x 7 production support you could [contact us](https://loodos.com/#letsconnect) with Commercial Offerings.

## Contributing

This is an open source project and we love to receive contributions from our community — you!

There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code.

## License

This is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/library/lds-autozip/blob/master/LICENSE)

## Acknowledgements

- [7-Zip](https://www.7-zip.org/)
