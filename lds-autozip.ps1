#usage .\lds-autozip.ps1 -beforeDays 90 -fileFormat lds-*.log -zipFileSuffix "lds" -backupFolderName "backup" -deleteAfter $False
Param(
	[Parameter(Mandatory=$False)]
	[Int32]$beforeDays=30,
	[Parameter(Mandatory=$False)]
	[String]$fileFormat="*.txt",
	[Parameter(Mandatory=$False)]
	[String]$backupFolderName="backup",
	[Parameter(Mandatory=$False)]
	[String]$zipFileSuffix="",
	[Parameter(Mandatory=$False)]
	[Boolean]$deleteAfter=$False,
	[Parameter(Mandatory=$False)]
	[String]$sevenZipExePath="C:\Program Files\7-Zip\7z.exe"
) 

$limit = (Get-Date).AddDays(-$beforeDays)

if (![string]::IsNullOrEmpty($zipFileSuffix)){
	$zipFileSuffix = "-" + $zipFileSuffix
}

$groups = Get-ChildItem -Path ".\" -Recurse -File -Filter $fileFormat | 
Where-Object { !$_.PSIsContainer -and $_.LastwriteTime -lt $limit } | 
Group-Object -Property {$_.LastwriteTime.tostring("yyyy-MM")},{$_.Directory.FullName} |
Select-Object @{n='GroupDate'; e={ $_.Values[0] }},
							@{n='RootDirectory'; e={ $_.Values[1] }},
							@{n='Files'; e={ $_.Group.FullName }}

ForEach ($group in $groups) {
	
	# create lst file for files to be zip.
	$filelistPath = $group.RootDirectory + "\" + $backupFolderName + "-" + $group.GroupDate + $zipFileSuffix + ".lst"
	Set-Content -Value $group.Files -Path $filelistPath

	# group zip file path
	$zipfile = $group.RootDirectory + "\" + $backupFolderName + "\" + $group.GroupDate + $zipFileSuffix + ".zip"
	
	# zipping
	& $sevenZipExePath a -mx9 -tzip $(If($deleteAfter){"-sdel"}) ("{0}" -f $zipfile) ("@{0}" -f $filelistPath)

	# remove lst file
	Remove-Item $filelistPath

	Write-Host "-----------------------------------------------------"
}
