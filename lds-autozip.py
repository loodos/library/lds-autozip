#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import collections
from datetime import datetime, date, timedelta
import re
import fnmatch
import subprocess
import logging

class Group():
    def __init__(self, group_date):
        self.RootDirectory = ""
        self.Files = []
        self.GroupDate = group_date # yyyy-MM
        self.FileListPath = ""
        self.ZipFileName = ""

    def create_filelist(self):
        backupFolderName = args.backupFolderName
        if not backupFolderName.startswith('/'):
            backupFolderName = args.path + "/" + backupFolderName

        self.FileListPath = backupFolderName + "-" + args.zipFilePrefix + self.GroupDate + args.zipFileSuffix + ".lst"
        self.ZipFileName = backupFolderName + "/" + args.zipFilePrefix + self.GroupDate + args.zipFileSuffix + ".zip"
        logging.info("{0} creating file list. ZipFileName: {2}. FileListPath:{1} # ".format(self.GroupDate, self.FileListPath, self.ZipFileName))
        with open(self.FileListPath, 'w') as f:
            for file in self.Files:
                f.write("%s\n" % file)
    
    def delete_filelist(self):
        logging.info("{0} deleting file list. FileListPath:{1} # ".format(self.GroupDate, self.FileListPath))
        os.remove(self.FileListPath)
    
    def compress(self):
        logging.info("{0} compressing".format(self.GroupDate))
        self.create_filelist()
        subprocess_args = ['7z', 'a', '-mx9', '-tzip']

        subprocess_args.append(self.ZipFileName)
        subprocess_args.append("-ir@" + self.FileListPath)

        if(args.deleteAfter):
            subprocess_args.append("-sdel")

        logging.debug(' '.join(str(x) for x in subprocess_args))
        subprocess.call(subprocess_args)

        self.delete_filelist()


def grouping(files):
    logging.info("grouping files")
    groupsDict = dict()
    for file_name in files:
        file_date = datetime.fromtimestamp(os.path.getmtime(file_name))
        group_date = file_date.strftime(args.fileGroupFormat)
        if not group_date in groupsDict:
            groupsDict[group_date] = Group(group_date)

        groupsDict[group_date].Files.append(file_name)
    
    return groupsDict
  
def compress_groups(groupsDict):
    logging.info("compress_groups")
    for key, group in groupsDict.iteritems():
        group.compress()
    
def filter_by_date(src_folder, filter, archive_date):
    files = []
    logging.info("searching: {0}".format(src_folder))
    for file_name in os.listdir(src_folder):
        full_name = os.path.join(src_folder, file_name)
        if os.path.isfile(full_name):
            logging.debug("file found: {0}".format(full_name))
            if fnmatch.fnmatch(file_name, filter):
                if datetime.fromtimestamp(os.path.getmtime(full_name)) < archive_date:
                    files.append(full_name)
    logging.info("{0} files found".format(len(files)))
    return files


def main():
    logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)
    logging.info("STARTING")
    endDay = date.today() - timedelta(days=args.beforeDays)
    endDay = datetime.combine(endDay, datetime.min.time())
    logging.info("End Day: {0}".format(endDay))
    files = filter_by_date(args.path, args.filter, endDay)
    groupsDict = grouping(files)
    compress_groups(groupsDict)


def is_valid_dir(parser, arg):
    """
    Check if arg is a valid directory that already exists on the file system.

    Parameters
    ----------
    parser : argparse object
    arg : str

    Returns
    -------
    arg
    """
    arg = os.path.abspath(arg)
    if not os.path.exists(arg):
        parser.error("The directory [%s] does not exist!" % arg)
    elif not os.path.isdir(arg):
        parser.error("[%s] not a directory!" % arg)
    else:
        return arg


def get_parser():    
    parser = argparse.ArgumentParser(prog='python log-auto.py', usage='%(prog)s [options]')
    parser.add_argument("-p", "--path",
                        dest="path",
                        default="./",
                        type=lambda x: is_valid_dir(parser, x),
                        help="Path that contain files. Default Value: './'",
                        metavar="DIR")
    parser.add_argument("-bd", "--beforeDays",
                        dest="beforeDays",
                        default=30,
                        type=int,
                        help="Default Value: 30")
    parser.add_argument("-f", "--filter",
                        dest="filter",
                        default="*.log",
                        help="Filter. Default Value: '*.log'")
    parser.add_argument("-bfn", "--backupFolderName",
                        dest="backupFolderName",
                        default="backup",
                        help="Backup folder name. Default Value: 'backup'")
    parser.add_argument("-zfp", "--zipFilePrefix",
                        dest="zipFilePrefix",
                        default="",
                        help="")
    parser.add_argument("-zfs", "--zipFileSuffix",
                        dest="zipFileSuffix",
                        default="",
                        help="")
    parser.add_argument("-da", "--deleteAfter",
                        dest="deleteAfter",
                        default=False,
                        help="Delete files after compress. Default Value: False")
    parser.add_argument("-fgf", "--fileGroupFormat",
                        dest="fileGroupFormat",
                        default='%Y-%m',
                        help="Creates a zip file for file groups. Ex: When value '%%Y-%%m' it will create one zip file for a month. When value '%%Y-%%m' it will create one zip file for a day. Default Value: '%%Y-%%m' (monthly)")

    return parser



if __name__ == "__main__":
    args = get_parser().parse_args()
    main()